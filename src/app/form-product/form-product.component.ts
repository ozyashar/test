import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {NgForm} from '@angular/forms';
import {Product} from '../product/product'

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

    product:Product = { categoryId:null, cost:null , pid: 0 , pname: ''};;


  @Output() productAddedEvent = new EventEmitter<Product>();



  onSubmit(form:NgForm){
    console.log(this.product);
    this.product = { categoryId: 0, cost:this.product.cost , pid: Math.round(Math.random()) , pname:this.product.pname};
  this.productAddedEvent.emit(this.product);
  this.product = { categoryId: 0, cost: null , pid: 0 , pname: ''};
}

constructor() { }

  ngOnInit() {
  }

}