import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Invoice} from '../invoice/invoice';  // ex 6 class
import {NgForm} from '@angular/forms'; // ex 6 class


@Component({
  selector: 'hadardasim-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {
 @Output() invoiceAddedEvent = new EventEmitter<Invoice>();  // ex 6 class
  invoice:Invoice = {pname:'', amount:''};  // ex 6 class

   onSubmit(form:NgForm){  // ex 6 class
   // console.log('it worked!');   ex 6 class, to check if the button 'submit' working fine
   console.log(form);
   this.invoiceAddedEvent.emit(this.invoice);
   this.invoice = {
     pname:'',
     amount:''
   }
  }

  constructor() { }

  ngOnInit() {
  }
}
