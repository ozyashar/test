import { Injectable } from '@angular/core';
import {Http} from '@angular/http';
import {AngularFire} from 'angularfire2';  // ex 7 class
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/delay';

@Injectable()
export class CustomersService {
 // private _url = 'http://jsonplaceholder.typicode.com/users';    ex 7 class

customersObservable;    /// ex 7 class
  getCustomers(){
   /// return this._http.get(this._url).map(res => res.json()).delay(2000); ex 7 class delete this // ex 7 class get the users from firebase
                  
   //this.customersObservable = this.af.database.list('/customers');                     
  // return this.customersObservable; 
    
        this.customersObservable = this.af.database.list('/customers').map(      /// use this for the join
      customers =>{
        customers.map(
          customer => {
            customer.prodCate = [];
            for(var p in customer.category){
                customer.prodCate.push(
                this.af.database.object('/category/' + p)
              )
            }
          }
        );
        return customers;
      }
    )
     return this.customersObservable;   
     
 	}
   




  
 // updateUser(user){
  //  let user1 = {name:user.name,email:user.email}  // from the ex with the cancel -> conected to users.component.ts
  //}

  addCustomer(customer){       // ex 7 class
    this.customersObservable.push(customer);
  }
  updateCustomer(customer){     ///// ex 7 class      after that changes in users.component.ts
    let customerKey = customer.$key;
    let customerData = {pname:customer.pname, cost:customer.cost};
    this.af.database.object('/customers/' + customerKey).update(customerData);
  }
  deleteCustomer(customer){                      //// ex 7 class     from users.component.ts
    let customerKey = customer.$key;
    this.af.database.object('/customers/' + customerKey).remove();
  }
  constructor(private af:AngularFire) { }   ////   private _http:Http  is was like that in ex 6 and changed in ex 7 class

}
