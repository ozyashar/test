import { Component, OnInit } from '@angular/core';
import { CustomersService } from './customers.service';

@Component({
  selector: 'hadardasim-customers',
  templateUrl: './customers.component.html',
  styles: [`
    .customers li { cursor: default; }
    .customers li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover,
    .list-group-item.active:focus { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class CustomersComponent implements OnInit {

  isLoading:Boolean = true;
  customers;

  currentCustomer;

  select(customer){
    this.currentCustomer = customer;
  }


  constructor(private _customersService:CustomersService) { }

 // addUser(user){  //ex 6 class             ex 7 class - deleting this beacuse of fire base
   // this.users.push(user)
 // }

    addCustomer(customer){                         /// ex 7 class    create this + the one in service for add a new user to fire base
      this._customersService.addCustomer(customer);
    }
    updateCustomer(customer){                              // ex 7 class     beafore that there were changes in users.service.ts
      this._customersService.updateCustomer(customer);          // until now 2 changes to create updateuser
    }

/*                      delete this in ex 7 class
  deleteUser(user){
    this.users.splice(
      this.users.indexOf(user),1
    ) 
}
*/
    deleteCustomer(customer){                             /// create this in ex 7 class  and after that users.service.ts
      this._customersService.deleteCustomer(customer);
    }
/*                              delete that in ex 7 class when updating a user
  cancel(user){  // for ex 5
    //this._usersService.updateUser(user); why to use 'usersService' ???
    let user1 = {name:user.name,email:user.email}; 
    // this.users.splice(
    // this.users.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
  // )
    console.log(this.users);
  }
*/

  ngOnInit() {
    this._customersService.getCustomers().subscribe(customersData =>
      {this.customers = customersData;
        this.isLoading = false
          console.log(this.customers)});      // ex 7 class
  }

}
