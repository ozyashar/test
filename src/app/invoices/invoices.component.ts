import { Component, OnInit } from '@angular/core';
import { InvoicesService } from './invoices.service';


@Component({
  selector: 'hadardasim-invoices',
  templateUrl: './invoices.component.html',
styles: [`
    .invoices li { cursor: default; }
    .invoices li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover,
    .list-group-item.active:focus { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]})
export class InvoicesComponent implements OnInit {
isLoading:Boolean = true;
  invoices;

  currentInvoice;

  select(invoice){
    this.currentInvoice = invoice;
  }


  constructor(private _invoicesService:InvoicesService) { }

 // addInvoice(invoice){  //ex 6 class             ex 7 class - deleting this beacuse of fire base
   // this.invoices.push(invoice)
 // }

    addInvoice(invoice){                         /// ex 7 class    create this + the one in service for add a new invoice to fire base
      this._invoicesService.addInvoice(invoice);
    }
    updateInvoice(invoice){                              // ex 7 class     beafore that there were changes in invoices.service.ts
      this._invoicesService.updateInvoice(invoice);          // until now 2 changes to create updateinvoice
    }

/*                      delete this in ex 7 class
  deleteInvoice(invoice){
    this.invoices.splice(
      this.invoices.indexOf(invoice),1
    ) 
}
*/
    deleteInvoice(invoice){                             /// create this in ex 7 class  and after that invoices.service.ts
      this._invoicesService.deleteInvoice(invoice);
    }
/*                              delete that in ex 7 class when updating a invoice
  cancel(invoice){  // for ex 5
    //this._invoicesService.updateInvoice(invoice); why to use 'invoicesService' ???
    let invoice1 = {pname:invoice.pname,amount:invoice.amount}; 
    // this.invoices.splice(
    // this.invoices.indexOf(originalAndEdited[0]),1,originalAndEdited[1]  
  // )
    console.log(this.invoices);
  }
*/

  ngOnInit() {
    this._invoicesService.getInvoices().subscribe(invoicesData =>
      {this.invoices = invoicesData;
        this.isLoading = false
          console.log(this.invoices)});      // ex 7 class
  }
}
