import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {Invoice} from './invoice';

@Component({
  selector: 'hadardasim-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.css'],
    inputs:['invoice']

})
export class InvoiceComponent implements OnInit {

   invoice:Invoice;
  tempInvoice:Invoice = {amount:null,pname:null};  // for ex 5        
  @Output() deleteEvent = new EventEmitter<Invoice>();
  @Output() cancelEvent = new EventEmitter<Invoice>(); // for ex 5 []
  isEdit:Boolean = false;
  editButtonText = "Edit";
  constructor() { }
  sendDelete(){
    this.deleteEvent.emit(this.invoice);
  }
  toggelEdit(){
    this.isEdit = !this.isEdit;
    this.isEdit ? this.editButtonText = "save" : this.editButtonText = "Edit";

         if(this.isEdit){   /// for ex 5          
       this.tempInvoice.amount = this.invoice.amount;
       this.tempInvoice.pname = this.invoice.pname;
     } else {
      // let originalAndNew = [];
      // originalAndNew.push(this.tempInvoice,this.invoice);
       this.cancelEvent.emit(this.invoice);  // need explanation ####
     } 

  }
  
  onCancel(){ // for ex 5
    this.isEdit = false;
    this.invoice.amount = this.tempInvoice.amount;
    this.invoice.pname = this.tempInvoice.pname;
    this.editButtonText = 'Edit';
  }
  ngOnInit() {
  }


}
